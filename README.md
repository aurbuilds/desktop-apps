# desktop-apps

A variety of desktop apps pre-built from AUR:
gnome-shell-extension-dash-to-panel
keeweb
wxhexeditor
masterpdfeditor-free
zoom
rclone-browser
google-chrome
virtualbox-ext-oracle


# Usage
Add the following lines to `/etc/pacman.conf`:

    [aurbuilds-desktop-apps]
    SigLevel = Never
    Server = https://aurbuilds.gitlab.io/desktop-apps/$arch